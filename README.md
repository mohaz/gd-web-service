# GDSERVICE


GDSERVICE it's simply API, build with nodejs to access a multi webservices


### Tech

GDSERVICE uses a number of open source projects to work properly:

* [node.js] - evented I/O for the backend
* [Express] - fast node.js network app framework [@tjholowaychuk]
* [jsonwebtoken] - security authentification
* [mongodb] - database nosql
* [cheerio] - alternative to XPATH


### Installation

Install the dependencies start the server.

```sh
$ git clone https://gitlab.com/mohaz/gd-web-service.git
$ cd gd-web-service
$ npm install 
$ node index.js
```
WARNING : don't forget to change .env.example to .env and set you environnement.


### API DOCUMENTATION WITH DEMO

You can access to the demo at this uri :  https://dev1.azerlab.fr


##### Create user

curl -X POST -k -H 'Content-Type: application/json' -i 'https://dev1.azerlab.fr/api/user/register' --data '{
"name" : "devtest",
"email" : "devtest@test.com",
"password" : "devtest"
}'

##### Login user 

curl -X POST -k -H 'Content-Type: application/json' -i 'https://dev1.azerlab.fr/api/user/login' --data '{
"email" : "devtest@test.com",
"password" : "devtest"
}'

this request return JWT and set it to header "auth-token"

##### use GDService

curl -X POST -k -H 'Accept: application/json' -H 'auth-token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZTJlOWY4ZjMzNjk1NDFiZGI0MGIxYjMiLCJpYXQiOjE1ODAxMTUyOTZ9.3PdnaKAzqUFQ4U8KuYBekjrDP9io32_480e77ux20Ng' -H 'Content-Type: application/json' -i 'https://dev1.azerlab.fr/api/service' --data '{
"uri" : "https://gitlab.com/"
}'

return this :
```json
{"charset":"UTF-8","title":"Google","description":"NULL","keywords":"NULL","copyright":"NULL","imagesCount":2,"images":["/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png","https://consent.google.com/status?continue=https://www.google.com&m=0&pc=s&timestamp=1580115353&gl=FR"],"linkCount":16,"links":["https://mail.google.com/mail/?tab=wm&ogbl","https://www.google.fr/imghp?hl=fr&tab=wi&ogbl","https://www.google.fr/intl/fr/about/products?tab=wh","https://accounts.google.com/ServiceLogin?hl=fr&passive=true&continue=https://www.google.com/","https://support.google.com/websearch/answer/106230?hl=fr","//support.google.com/websearch?p=fr_tos&hl=fr&fg=1","https://support.google.com/websearch?p=fr_consumer_info&hl=fr&fg=1","https://policies.google.com/privacy?fg=1","https://policies.google.com/terms?fg=1","https://www.google.com/preferences?hl=fr","https://www.google.com/preferences?hl=fr&fg=1","//support.google.com/websearch/?p=ws_results_help&hl=fr&fg=1","https://www.google.com/intl/fr_fr/ads/?subid=ww-ww-et-g-awa-a-g_hpafoot1_1!o2&utm_source=google.com&utm_medium=referral&utm_campaign=google_hpafooter&fg=1","https://www.google.com/services/?subid=ww-ww-et-g-awa-a-g_hpbfoot1_1!o2&utm_source=google.com&utm_medium=referral&utm_campaign=google_hpbfooter&fg=1","https://about.google/?utm_source=google-FR&utm_medium=referral&utm_campaign=hp-footer&fg=1","//google.com/search/howsearchworks/?fg=1"],"linksNoAlt":16,"linksAlt":0}
```






