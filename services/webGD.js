var request = require('request');
var cheerio = require('cheerio');
var keywords = "";
var desc = "";
var copyright = "";
var img = [];
var lnk = [];
var lnkalt = [];
var lnknoalt = [];


const promisifiedRequest = function (options) {
    return new Promise((resolve, reject) => {
        request(options, (error, response, body) => {
            if (response) {
                return resolve(response);
            }
            if (error) {
                return reject(error);
            }
        });
    });
};

module.exports = async (url) => {
    const options = {
        url: 'https://www.google.com',
        method: 'GET',
        gzip: true,
        headers: {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.96 Safari/537.36'
        }
    };

    let response = await promisifiedRequest(options);
    $ = cheerio.load(response.body);
    var title = $('title').text();
    var charset = $('meta').attr('charset');
    $('meta').each(function () {
        var name = $(this).attr('name');
        switch (name) {
            case "description":
                desc = $(this).attr('content');
                break;
            case "keywords":
                keywords = $(this).attr('content');
                break;
            case "copyright":
                copyright = $(this).attr('content');
                break;
        }
    });
    $('img').each(function () { // get all images of the site
        var image = $(this).attr('src');
        if ((image !== undefined)) {
            img.push(image);
        }
    });
    $('a').each(function () { // get all links from the site
        var link = $(this).attr('href');
        if ((link !== undefined) && (link.substr(0, 2) == "//" || link.substr(0, 4) == "http")) {
            lnk.push(link);
            if ($(this).attr('alt') !== undefined) {
                lnkalt.push(link);
            } else {
                lnknoalt.push(link);
            }
        }
    });

    return {
        charset: (charset == undefined ? "NULL" : charset),
        title: (title == undefined ? "NULL" : title),
        description: (desc == "" ? "NULL" : desc),
        keywords: (keywords == "" ? "NULL" : keywords),
        copyright: (copyright == "" ? "NULL" : copyright),
        imagesCount: img.length,
        images: img,
        linkCount: lnk.length,
        links: lnk,
        linksNoAlt: lnknoalt.length,
        linksAlt: lnkalt.length
    };

};

