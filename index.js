const express = require('express');
const app = express();
const mongoose = require('mongoose');
const dotenv = require('dotenv');

//Import Routes
const authRoute = require('./routes/auth');
const gdServiceRoute = require('./routes/gdService');

dotenv.config();

//Connect to DB
mongoose.connect(
    process.env.DB_CONNECT,
    { useNewUrlParser: true},
    () => console.log('connect to DB')
);

//Middleware
app.use(express.json());

//Routes Middlewares
app.use('/api/user', authRoute);
app.use('/api/service', gdServiceRoute);

app.listen(3000, () => console.log('server is ok !'));