const routes = require('express').Router();
const verify =  require('../middleware/verifyToken');
const webGD = require('../services/webGD');
const { uriValidation} = require('../validation');

routes.post('/', verify, async (req, res) => {
    //validate data
    const { error } = uriValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const data = await webGD(req.body.url);
    res.json(data);
});


module.exports = routes;